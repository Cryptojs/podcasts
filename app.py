from flask import Flask , request, jsonify
from requests import get
import json

app = Flask(__name__)
pathApi         = "https://rss.itunes.apple.com/api/v1/us/podcasts/top-podcasts/all/100/explicit.json"
pathApiTwenty   = "https://rss.itunes.apple.com/api/v1/us/podcasts/top-podcasts/all/20/explicit.json"
pathApiTwentyES = "https://rss.itunes.apple.com/api/v1/sv/podcasts/top-podcasts/all/20/explicit.json"

@app.route('/allPodcasts', methods=["GET"])
def index():
    podcasts =  get(pathApi).json()
    return jsonify({"message": " Podcasts list", "results": podcasts['feed']['results']})

@app.route('/allPodcastsJson', methods=["GET"])
def indexJson():
    with open('data.json') as file:
        data = json.load(file)
    return jsonify({"message": " Podcasts list", "results": data})

@app.route('/searchPodcast', methods=["GET"])
def searchPodcast():
    name          = request.json['name']
    podcasts      =  get(pathApi).json()
    podcastsFound = [podcast for podcast in podcasts['feed']['results'] if podcast['name'] == name]

    if ( len(podcastsFound) > 0):
        return jsonify({"message": "Podcasts results", "podcasts":podcastsFound})
    return jsonify({"message": "Podcast not found"})

@app.route('/searchPodcastJson', methods=["GET"])
def searchPodcastJson():
    name  = request.json['name']

    with open('data.json') as file:
        data = json.load(file)
    
    podcastsFound = [podcast for podcast in data['resultado'] if podcast['name'] == name]

    if ( len(podcastsFound) > 0):
        return jsonify({"message": "Podcasts results", "podcasts":podcastsFound})
    return jsonify({"message": "Podcast not found"})

@app.route('/groupPodcastByGenre', methods=["GET"])
def groupPodcastByGenres():
    name = request.json['genreName']
    podcasts =  get(pathApi).json()
    
    #search podcasts by genreName 
    podcastsFound = [podcast for podcast in podcasts['feed']['results'] for podcastsGenres in podcast['genres'] if podcastsGenres['name'] == name]

    if ( len(podcastsFound) > 0):
        return jsonify({"message": "Podcasts results", "podcasts":podcastsFound})
    return jsonify({"message": "Not found data to show"})

@app.route('/addPodcasts', methods=["POST"])
def addPodcasts():
    podcasts =  get(pathApiTwenty).json()
    data = {'resultado' : podcasts['feed']['results']}
    with open('data.json', 'w') as file:
        json.dump(data, file, indent=4)
    return jsonify({"message": "Podcast added successfully", "podcasts list": data})

@app.route('/deletePodcast', methods=["DELETE"])
def deletePodcast():
    id = request.json['id']
    with open('data.json') as file:
        data = json.load(file)
    podcastsFound = [podcast for podcast in data['resultado'] if podcast['id'] == id]
    if ( len(podcastsFound) > 0):
        data['resultado'].remove(podcastsFound[0])
        with open('data.json', 'w') as file:
            json.dump(data, file, indent=4)

        return jsonify({"message": "Podcast deleted successfully", "podcasts list": data})
    return jsonify({"message": "Podcast not found"})

@app.route('/replacePodcasts', methods=["PUT"])
def replacePodcast():
    podcasts =  get(pathApiTwentyES).json()
    contenedor = len(podcasts['feed']['results'])
    
    with open('data.json') as file:
        data = json.load(file)

    results = []
    if len(data['resultado']) >= 19:
        for podcast in range(len(data['resultado'])-1, -20 ,-1):
            contenedor -= 1
            data['resultado'][podcast] = podcasts['feed']['results'][contenedor]

        with open('data.json', 'w') as file:
            json.dump(data, file, indent=4)

        return jsonify({"message": 'data replace successfully', "results" : data['resultado']})
    return jsonify({"message": 'data not replace', "results" : data['resultado']})
    
if __name__ == '__main__':
    app.run(debug=True)