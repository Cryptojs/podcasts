# Python FLASK REST API
challenge: Python 3 REST API through SQLite.

``` Steps: ```
### Git Clone 
``` 
git clone https://gitlab.com/Cryptojs/podcasts.git
```
### Change Directory
```
cd podcasts/
```
### Note : Install Python 3 and Postman for your system, but you can use Insomnia: https://insomnia.rest/download/
### Install virtualenv:
```
pip3 install virtualenv
```
### Active virtualenv:
```
source bin/activate
```
### Install request:
```
pip install request
```
### Start Server
```
python3 app.py
```
### Routes to get all Podcast API & Json File
```
/allPodcasts           : GET
/allPodcastsJson       : GET
```
### Routes to get all Podcast API & Json File
```
/searchPodcast         : GET
/searchPodcastJson     : GET
```

### Routes to group Podcasts API
```
/groupPodcastByGenre   : GET
```

### Routes to add Podcasts to Json File
```
/addPodcasts           : POST
```
### Routes to delete a Podcast Json File
```
/deletePodcast         : DELETE
```
### Routes to replace 20 Podcasts Json File
```
/replacePodcasts       : PUT
```